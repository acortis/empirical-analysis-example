﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpiricalAnalysis
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();

            List<int> arrayBasedVector = new List<int>();

            // Measure speed of arrayBasedVector operation with an arrayBasedVector of different sizes
            int[] sizes = new int[] { 2000, 3000, 4000, 5000, 6000, 7000, 8000 };

            // for every problem size in the list of problem sizes
            foreach (int size in sizes)
            {
                sw.Reset(); // set stopwatch to 0

                // Build an array based vector of size
                for (int i = 0; i < size; i++)
                {
                    arrayBasedVector.Add(i);
                }

                // Execute the method once before timing...
                arrayBasedVector.Add(1000); // Append a value of 1000
                arrayBasedVector.RemoveAt(arrayBasedVector.Count - 1); // remove last element

                int repetitions = 10000;

                // for the number of repetitions required
                for (int i = 0; i < repetitions; i++)
                {
                    sw.Start();
                    arrayBasedVector.Add(1000);  // time the method
                    sw.Stop();

                    arrayBasedVector.RemoveAt(arrayBasedVector.Count - 1);  // clean up - remove the extra item!
                }

                // Modern versions of the .Net Framework do not require String.Format
                Console.WriteLine("For an array of size {0}:", size);
                Console.WriteLine("The total time for {0} repetitions is {1} ticks", repetitions, sw.ElapsedTicks);
                Console.WriteLine("The mean (average) time for a single operation is {0}", (double)sw.ElapsedTicks/repetitions);
            }

            Console.ReadKey();

        }
    }
}
